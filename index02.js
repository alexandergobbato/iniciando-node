const express   = require('express');
const port      = 8000;
let app         = express();

app.get('/', (req, res)=>{
    res.send("Resultdo sucesso RAÍZ");
    console.log("GET /");
});

app.get('/clientes', (req, res)=>{
    let obj = req.query;
    res.send("Resultdo sucesso /CLIENTES " + obj.nome);
    console.log("GET /CLIENTES");
});

app.get('/clientes/nome/:name', (req, res)=>{
    console.log(`REQ: ${req.params}`);
    console.log(`REQ: ${req.params.name}`);
    let nome = req.params.name;
    res.send("Resultdo sucesso /CLIENTES " + nome);
    console.log("GET /CLIENTES param");
});

const bodyParser = require('body-parser');
app.use(bodyParser.json()) 
// for parsing application/json

app.use(bodyParser.urlencoded({ extended: true })) 
// for parsing application/x-www-form-urlencoded

app.get('/clientes/body', (req, res)=>{
    console.log(`REQ: ${(req.body)}`);

    let obj = req.body;
    res.send("Resultdo sucesso /CLIENTES " + obj.nome + " - " + obj.sobrenome );
    console.log("GET /CLIENTES body");
});

app.listen(port,()=>{
    console.log(`projeto executando na porta ${port}`);
});
